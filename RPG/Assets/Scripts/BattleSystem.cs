﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public enum BattleState
{
    START,
    PLAYERTURN,
    ENEMYTURN,
    WAIT,
    WIN,
    LOST
}

public enum MagicType
{
    None,
    Fireball,
    Blizzaga,
    Lightning
}

public class BattleSystem : MonoBehaviour
{

    public BattleState state;
    public MagicType magicCall;

    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject gameOver;
    public GameObject battleSys;
    public GameObject questlog;

    public ChacterMovement charm;

    public GameObject magicBox;

    public Transform playerBattleStation;
    public Transform enemyBattleStation;

    Entity playerEntity;
    Entity enemyEntity;

    public TextMeshProUGUI status;

    public BattleHUD playerHUD;
    public BattleHUD enemyHUD;

    public ChacterMovement player;
    public Player_Quest pq;

    private int magicCharges = 3;

    private bool criticalHit = false;
    private bool fizzle = false;


    // Start is called before the first frame update
    void Awake()
    {
        state = BattleState.START;
        StartCoroutine(setUpBattle());
        questlog.SetActive(false);
    }

    IEnumerator setUpBattle()
    {
        GameObject playerStats = Instantiate(playerPrefab, playerBattleStation);
        playerEntity = playerStats.GetComponent<Entity>();

        GameObject enemyStats = Instantiate(enemyPrefab, enemyBattleStation);
        enemyEntity = enemyStats.GetComponent<Entity>();

        status.SetText("A wild " + enemyEntity.entityName + " approaches...");
        questlog.SetActive(false);

        playerHUD.setHUD(playerEntity);
        enemyHUD.setHUD(enemyEntity);

        yield return new WaitForSeconds(2f);

        state = BattleState.PLAYERTURN;
        magicCall = MagicType.None;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        status.SetText("Choose an action... ");
    }

    public void OnAttackButton()
    {
        status.enabled = true;
        magicBox.SetActive(false);
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void OnHealButton()
    {
        status.enabled = true;
        magicBox.SetActive(false);
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        if (playerEntity.checkMagicCharges() <= 0)
        {
            status.SetText("Not enough mana");
            return;
        }

        
        StartCoroutine(PlayerHeal());
    }

    public void onFireballButton()
    {
        status.enabled = true;
        magicBox.SetActive(false);
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        if (playerEntity.checkMagicCharges() <= 0)
        {
            status.SetText("Not enough mana");
            return;
        }

        magicCall = MagicType.Fireball;
        StartCoroutine(PlayerAttackMagic(1));
    }
    
    public void onBlizzagaButton()
    {
        status.enabled = true;
        magicBox.SetActive(false);
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        if (playerEntity.checkMagicCharges() <= 0)
        {
            status.SetText("Not enough mana");
            return;
        }

        magicCall = MagicType.Blizzaga;
        StartCoroutine(PlayerAttackMagic(2));
    }
    
    public void onLightningButton()
    {
        status.enabled = true;
        magicBox.SetActive(false);
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        if (playerEntity.checkMagicCharges() <= 0)
        {
            status.SetText("Not enough mana");
            return;
        }

        magicCall = MagicType.Lightning;
        StartCoroutine(PlayerAttackMagic(3));
    }

    public void onMagicButton()
    {
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        magicBox.SetActive(true);
        status.enabled = false;
    }

    IEnumerator PlayerAttack()
    {
        bool isDead;

        playerEntity.isAttacking();
        criticalHit = checkCriticalHit();
        if (criticalHit)
        {
            isDead = enemyEntity.takeDamage(playerEntity.damage * 2);
        }
        else
        {
            isDead = enemyEntity.takeDamage(playerEntity.damage);
        }
        state = BattleState.WAIT;

        enemyHUD.setHP(enemyEntity.currentHP, enemyEntity);

        if (criticalHit)
        {
            status.SetText("Your attack critically hit for " + playerEntity.damage * 2 + " damage");
            criticalHit = false;
        }
        else
        {
            status.SetText("Your attack hit for " + playerEntity.damage + " damage");
        }

        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            state = BattleState.WIN;
            endBattle();
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator PlayerHeal()
    {
        playerEntity.usedMagic();
        playerEntity.heal(20);
        state = BattleState.WAIT;

        playerHUD.setHP(playerEntity.currentHP, playerEntity);
        status.SetText("You feel refreshed");
        yield return new WaitForSeconds(2f);

        state = BattleState.ENEMYTURN;
        StartCoroutine(EnemyTurn());
    }

    IEnumerator PlayerAttackMagic(int magicType)
    {
        bool isDead = false;

        playerEntity.usedMagic();
        playerEntity.isAttacking();
        fizzle = checkFizzle();
        if (fizzle)
        {
            switch(magicType)
            {
                case 1:
                    status.SetText("Your Fireball fizzled");
                    break;
                case 2:
                    status.SetText("Your Blizzaga fizzled");
                    break;
                case 3:
                    status.SetText("Your call to Thor fails");
                    break;
                default:
                    status.SetText("something went wrong lol");
                    break;
            }
        }
        else
        {
            switch (magicType)
            {
                case 1:
                    isDead = enemyEntity.takeDamage(20);
                    status.SetText("You throw a spicy Fireball");
                    break;
                case 2:
                    isDead = enemyEntity.takeDamage(30);
                    status.SetText("You throw a frosty Snowball");
                    break;
                case 3:
                    isDead = enemyEntity.takeDamage(40);
                    status.SetText("Odin smiles upon you");
                    break;
                default:
                    status.SetText("something went wrong lol");
                    break;
            }
        }

        state = BattleState.WAIT;
        enemyHUD.setHP(enemyEntity.currentHP, enemyEntity);
        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            state = BattleState.WIN;
            endBattle();
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator EnemyTurn()
    {
        enemyEntity.isAttacking();
        status.SetText(enemyEntity.entityName + " attacks you for " + enemyEntity.damage + " damage");
        yield return new WaitForSeconds(1f);

        bool isDead = playerEntity.takeDamage(enemyEntity.damage);
        playerHUD.setHP(playerEntity.currentHP, playerEntity);
        yield return new WaitForSeconds(1f);

        if (isDead)
        {
            state = BattleState.LOST;
            endBattle();
        }
        else
        {
            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
    }

    bool checkCriticalHit()
    {
        if (Random.Range(1, 100) <= 10)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool checkFizzle()
    {

        if (Random.Range(1, 100) <= 5 && magicCall == MagicType.Fireball)
        {
            return true;
        }
        else if(Random.Range(1, 100) <=  15 && magicCall == MagicType.Blizzaga)
        {
            return true;
        }
        else if (Random.Range(1, 100) <= 35 && magicCall == MagicType.Lightning)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void onceMore()
    {
        state = BattleState.PLAYERTURN;
        StartCoroutine(setUpBattle());
        questlog.SetActive(false);
        enemyEntity.resetEntity();
    }

    void endBattle()
    {
        if(state == BattleState.WIN)
        {
            status.SetText("You defeated " + enemyEntity.entityName);
            pq.quest5sc = true;
            playerEntity.resetMagic();
            if(charm.Boss())
            {
                SceneManager.LoadScene("Title");
            }
            StartCoroutine(backToGame());
        }
        else if (state == BattleState.LOST)
        {
            status.SetText("You have been defeated...");
            gameOver.SetActive(true);
        }
    }

    IEnumerator backToGame()
    {
        yield return new WaitForSeconds(2f);
        player.characterLock();
        onceMore();
        battleSys.SetActive(false);
        questlog.SetActive(true);
    }

    public void resetBattle()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void quitBattle()
    {
        SceneManager.LoadScene("Title");
    }
}
