﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatureManager : MonoBehaviour
{
    class Feature
    {
        public Feature(GameObject n, bool t)
        {
            name = n;
            active = t;
        }
        public Feature(GameObject n)
        {
            name = n;
            active = false;
        }
        public GameObject name;
        public bool active;
    }

    //Move
    public static GameObject feature1;
    //Move Camera
    public static GameObject feature2;
    //Background
    public static GameObject feature3;
    //Menu
    public static GameObject feature4;
    //Color
    public static GameObject feature5;
    //enemies
    public static GameObject feature6;
    //NPCs
    public static GameObject feature7;
    //NPC Sprites
    public static GameObject feature8;
    //Reach Final Boss
    public static GameObject feature9;
    //Character Sprite
    public static GameObject feature0;

    //classes with active as false if not defined
    Feature f1 = new Feature(feature1);
    Feature f2 = new Feature(feature2);
    Feature f3 = new Feature(feature3, true);
    Feature f4 = new Feature(feature4);
    Feature f5 = new Feature(feature5, true);
    Feature f6 = new Feature(feature6);
    Feature f7 = new Feature(feature7);
    Feature f8 = new Feature(feature8);
    Feature f9 = new Feature(feature9);
    Feature f0 = new Feature(feature0, true);

    // Update is called once per frame
    void Update()
    {
        f1.name.SetActive(f1.active);
        f2.name.SetActive(f2.active);
        f3.name.SetActive(f3.active);
        f4.name.SetActive(f4.active);
        f5.name.SetActive(f5.active);
        f6.name.SetActive(f6.active);
        f7.name.SetActive(f7.active);
        f8.name.SetActive(f8.active);
        f9.name.SetActive(f9.active);
        f0.name.SetActive(f0.active);
    }

    public void Reset()
    {
        f3.active = false;
        f5.active = false;
        f0.active = false;
    }
}
