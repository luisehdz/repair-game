﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    enum Direction
    {
        noDir,
        up,
        right,
        down,
        left
    }

    float vert = 10f;
    float hori = 8.9f*2f;
    int count = 0;
    Direction shouldMove = Direction.noDir;
    float cameraTempPos= -.123456f;
    public GameObject character;
    bool justMoved =  false;
    public bool cameraLocked = false;
    public ChacterMovement player;
    
    // Start is called before the first frame update
    public void cameraLock()
    {
        cameraLocked = !cameraLocked;
    }


    // Update is called once per frame
    void Update()
    {
        if (character.transform.position.x > gameObject.transform.position.x + hori/2f+.25f)
        {
            shouldMove = Direction.right;
        }
        if (character.transform.position.x < gameObject.transform.position.x - hori / 2f-.25f)
        {
            shouldMove = Direction.left;
        }
        if (character.transform.position.y > gameObject.transform.position.y + vert / 2f+.25f)
        {
            shouldMove = Direction.up;
        }
        if (character.transform.position.y < gameObject.transform.position.y - vert / 2f-.25f)
        {
            shouldMove = Direction.down;
        }
    }

    private void FixedUpdate()
    {
        if (!cameraLocked) {
            //Debug.Log("working");
            if (shouldMove != Direction.noDir)
            {
                if (shouldMove == Direction.up)
                {
                    if (cameraTempPos == -.123456f)
                    {
                        cameraTempPos = gameObject.transform.position.y;
                        player.characterLock();
                    }

                    gameObject.transform.Translate(new Vector3(0, vert, 0) * Time.deltaTime);


                    if (gameObject.transform.position.y > cameraTempPos + vert)
                    {
                        gameObject.transform.position.Set(gameObject.transform.position.x, gameObject.transform.position.y + vert, gameObject.transform.position.z);
                        cameraTempPos = -.123456f;
                        shouldMove = Direction.noDir;
                        player.characterLock();

                    }
                }
                if (shouldMove == Direction.right)
                {
                    if (cameraTempPos == -.123456f)
                    {
                        cameraTempPos = gameObject.transform.position.x;
                        player.characterLock();
                    }

                    gameObject.transform.Translate(new Vector3(hori,0,0) * Time.deltaTime);
                

                    if (gameObject.transform.position.x>cameraTempPos + hori)
                    {
                        gameObject.transform.position.Set(gameObject.transform.position.x + hori, gameObject.transform.position.y, gameObject.transform.position.z);
                        cameraTempPos = -.123456f;
                        shouldMove = Direction.noDir;
                        player.characterLock();
                    }
                }
                if (shouldMove == Direction.left)
                {
                    if (cameraTempPos == -.123456f)
                    {
                        cameraTempPos = gameObject.transform.position.x;
                        player.characterLock();
                    }

                    gameObject.transform.Translate(new Vector3(-hori, 0, 0) * Time.deltaTime);


                    if (gameObject.transform.position.x < cameraTempPos - hori)
                    {
                        gameObject.transform.position.Set(gameObject.transform.position.x - hori, gameObject.transform.position.y, gameObject.transform.position.z);
                        cameraTempPos = -.123456f;
                        shouldMove = Direction.noDir;
                        player.characterLock();
                    }
                }
                if (shouldMove == Direction.down)
                {
                    if (cameraTempPos == -.123456f)
                    {
                        cameraTempPos = gameObject.transform.position.y;
                        player.characterLock();
                    }

                    gameObject.transform.Translate(new Vector3(0, -vert, 0) * Time.deltaTime);


                    if (gameObject.transform.position.y < cameraTempPos - vert)
                    {
                        gameObject.transform.position.Set(gameObject.transform.position.x, gameObject.transform.position.y - vert, gameObject.transform.position.z);
                        cameraTempPos = -.123456f;
                        shouldMove = Direction.noDir;
                        player.characterLock();
                    }
                }
            }
        }
    }
    public bool CamMove()
    {
        return shouldMove != Direction.noDir;
    }
    public bool moveCount()
    {
        if (shouldMove != Direction.noDir && !justMoved)
        {
            count++;
            justMoved = true;
        }else if(shouldMove == Direction.noDir)
        {
            justMoved = false;
        }
        return count>2;
    }
}
