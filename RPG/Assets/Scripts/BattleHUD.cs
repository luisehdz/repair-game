﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BattleHUD : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI chargeText;
    public Slider hpSlider;

    public void setHUD(Entity info)
    {
        nameText.SetText(info.entityName);
        levelText.SetText("Lvl " + info.entityLevel);
        healthText.SetText(info.currentHP + "/" + info.maxHP);
        hpSlider.maxValue = info.maxHP;
        hpSlider.value = info.currentHP;
        chargeText.SetText(info.currentMagicCharges + "/" + info.maxMagicCharges);
    }

    public void setHP(int hp, Entity info)
    {
        hpSlider.value = hp;
        healthText.SetText(info.currentHP + "/" + info.maxHP);
        chargeText.SetText(info.currentMagicCharges + "/" + info.maxMagicCharges);
    }
}
