﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player_Quest : MonoBehaviour
{

    static int questFinished;

    //Text boxes for Quest
    public TextMeshProUGUI questTMP;
    public TextMeshProUGUI solutionTMP;

    //Adds Features to activate and deactivate
    public GameObject Wall1;
    public GameObject BG;
    public GameObject Wall2;
    public GameObject Wall3;
    public GameObject Enemies;
    public GameObject NPCs;
    public GameObject Boss;

    //Adds Condition varables for quest
    bool n=false;
    bool s = false;
    bool e = false;
    bool w = false;
    static bool swordCollision = false;
    static bool NPCCollision = false;
    static bool ElderCollision = false;
    static bool bossWall = false;
    static bool bossCollision = false;
    public bool quest5sc = false;
    public CameraMove cam;
    public ChacterMovement charm;
    public PlayerCollision c;
    public BattleSystem fight;

    private void Start()
    {
        questFinished = 0;
    }

    //Quest Class
    class Quest
    {
        public Quest(string n, string r, string p, bool t)
        {
            name = n;
            requarments = r;
            reward = p;
            compleate = t;
        }
        public void print(Text q, Text s)
        {
            q.text = name;
            s.text = requarments;
        }

        public void printTMP(TextMeshProUGUI q, TextMeshProUGUI s)
        {
            q.text = name;
            s.text = requarments;
        }

        string name;
        string requarments;
        string reward;
        public bool compleate;
    };

    //Quests
    //Format/Task/Reward
    Quest quest1 = new Quest("Error: \"Explore\" Script is unknown.", "Suggested Fix: Move in all four directions using WASD", "Unlocks New Land",false);
    Quest quest2 = new Quest("Error: Background and \"Menu\" Script are unknown.", "Suggested Fix: Move to a different part of the map", "Unlocks Menu", false);
    Quest quest3 = new Quest("Error: Color Asset is unknown.", "Travel to 3 different parts of the map", "Unlocks Background and Color", false);
    Quest quest4 = new Quest("Error: \"enemies\" prefab is missing.", "Suggested Fix: Find a sword", "Unlocks enemies", false);
    Quest quest5 = new Quest("Error: \"NPCs\" prefab is missing.", "Suggested Fix: Kill an enemy", "Unlocks NPCs", false);
    Quest quest6 = new Quest("Error: \"Speech\" Script is unknown.", "Suggested Fix: Talk to NPC", "Understand NPCs", false);
    Quest quest7 = new Quest("Error: \"Sprits\"\\enemies and \"Sprits\"\\NPCs are missing", "Suggested Fix: Talk to the Elder", "Adds Sprites to enemies and NPCs", false);
    Quest quest8 = new Quest("Error: \"Sprits\"\\player and \"questLog\" Script are Unknown", "Find The Blocked Temple", "Unlocks Character Sprite && Quest Log", false);
    Quest quest9 = new Quest("Questions", "Ask Elder about the Temple", "Unlocks Final Boss", false);
    Quest quest10 = new Quest("Final Fix", "Kill Final Boss", "Unlocks Dialog Box", false);
    //End Game = True

    // Update is called once per frame
    void Update()
    {
        FaceCheck();
        switch(questFinished)
        {
            case 0:
                quest1.printTMP(questTMP, solutionTMP);
                if (allDirections())
                {
                    quest1.compleate = true;
                    questFinished++;
                    Wall1.SetActive(false);
                }
                break;
            case 1:
                quest2.printTMP(questTMP, solutionTMP);
                if (cam.CamMove())
                {
                    quest2.compleate = true;
                    questFinished++; 
                }
                break;
            case 2:
                quest3.printTMP(questTMP, solutionTMP);
                if (cam.moveCount())
                {
                    Wall2.SetActive(false);
                    BG.SetActive(true);
                    quest3.compleate = true;
                    questFinished++;
                }
                break;
            case 3:
                quest4.printTMP(questTMP, solutionTMP);
                if (swordCollision)//collide with sword
                {
                    quest4.compleate = true;
                    questFinished++;
                    Enemies.SetActive(true);
                }
                break;
            case 4:
                quest5.printTMP(questTMP, solutionTMP);
                if (quest5sc)//battle sequece end
                { 
                    quest5.compleate = true;
                    questFinished++;
                    NPCs.SetActive(true);
                }
                break;
            case 5:
                quest6.printTMP(questTMP, solutionTMP);
                if (NPCCollision||ElderCollision)//collide with NPC or Elder
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        quest6.compleate = true;
                        ElderCollision = false;
                        questFinished++;
                    }
                }
                break;
            case 6:
                quest7.printTMP(questTMP, solutionTMP);
                Debug.Log(ElderCollision);
                if (ElderCollision)//collide with NPC with elder tag
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        quest7.compleate = true;
                        questFinished++;
                    }
                }
                break;
            case 7:
                quest8.printTMP(questTMP, solutionTMP);
                if (bossWall)//collide with invis collider
                {
                    quest3.compleate = true;
                    questFinished++;
                }
                break;
            case 8:
                quest9.printTMP(questTMP, solutionTMP);
                Debug.Log(ElderCollision);
                if (ElderCollision && Input.GetKeyDown(KeyCode.E))//collide with elder 
                {
                    quest3.compleate = true;
                    questFinished++;
                    Wall3.SetActive(false);
                    charm.anim.enabled = true;
                    Boss.SetActive(true);
                }
                break;
            case 9:
                quest10.printTMP(questTMP, solutionTMP);
                if (bossCollision && fight.state == BattleState.WIN)//collide with boss and battle squence ends
                {
                    quest3.compleate = true;
                    questFinished++;
                }
                break;/**/
            default:
                Debug.Log("You Win");
                break;
        }
    }

    bool allDirections()
    {
        bool condition = false;
        if (Input.GetKeyDown(KeyCode.UpArrow)|| Input.GetKeyDown(KeyCode.W))
            n = true;
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            s = true;
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            e = true;
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            w = true;
        if (n && s && e && w)
            condition = true;
        return condition;
    }

    void FaceCheck()
    {
        swordCollision = c.Sword();
        NPCCollision = c.NPC();
        ElderCollision = c.Elder();
        bossWall = c.Wall();
        bossCollision = c.Boss();
    }
    
}
