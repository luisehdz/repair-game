﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause_Canvas : MonoBehaviour
{
    public GameObject shadow;
    public GameObject menu;
    public GameObject player;

    private bool paused;

    // Start is called before the first frame update
    void Start()
    {
        shadow = GameObject.Find("GameCanvas/BGShadow");
        menu = GameObject.Find("GameCanvas/BGMenu");
        player = GameObject.Find("Basic Player");

        shadow.SetActive(false);
        menu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (paused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void ResumeGame()
    {
        paused = false;
        Time.timeScale = 1f;

        shadow.SetActive(false);
        menu.SetActive(false);
        player.SetActive(true);
    }

    void PauseGame()
    {
        paused = true;
        Time.timeScale = 0f;

        shadow.SetActive(true);
        menu.SetActive(true);
        player.SetActive(false);
    }

    public void QuitGame()
    {
        Debug.Log("You Quit");
        Application.Quit();
    }
}