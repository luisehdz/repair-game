﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrunkAI : MonoBehaviour
{
    private int direction;

    // Update is called once per frame
    void Update()
    {
        walkDrunkenly();
    }

    void walkDrunkenly()
    {
        direction = Random.Range(1, 4);

        switch(direction)
        {
            case 1:
                transform.Translate(new Vector3(0, 0.05f, 0));
                break;
            case 2:
                transform.Translate(new Vector3(0.05f, 0, 0));
                break;
            case 3:
                transform.Translate(new Vector3(0, -0.05f, 0));
                break;
            case 4:
                transform.Translate(new Vector3(-0.05f, 0, 0));
                break;
        }
    }
}
