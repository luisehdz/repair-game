﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChacterMovement : MonoBehaviour
{

    public float moveSpeed = 4f;
    public Rigidbody2D rb;
    public Vector2 movement;
    public bool movementLocked = false;

    bool bossHit = false;

    public Animator anim;
    public Animation fading;
    public AudioSource song;
    public AudioSource fieldtheme;
    public GameObject battle;
    public GameObject player;

    public void characterLock()
    {
        movementLocked = !movementLocked;
    }

    private State state;
    private enum State
    {
        Normal,
        NotMoving,
    }
    private void Awake()
    {
        state = State.Normal;
    }


    // Start is called before the first frame update
    void Start()
    {
        battle.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Normal:
                moveSpeed = 10f;
                Sprint();
                Dab();
                break;

            case State.NotMoving:
                NotMoving();
                break;
        }

        Vector2 moveInput = new Vector2(movement.x = Input.GetAxisRaw("Horizontal"), movement.y = Input.GetAxisRaw("Vertical"));
        movement = moveInput.normalized * moveSpeed;

        if (movement.magnitude > 0)
        {
            anim.SetFloat("Horizontal", movement.x);
            anim.SetFloat("Vertical", movement.y);
        }
        anim.SetFloat("Speed", movement.sqrMagnitude);

    }

    void FixedUpdate()
    {
        if (!movementLocked)
        {
            rb.MovePosition(rb.position + movement * Time.fixedDeltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Enemy" || collision.collider.tag == "Boss")
        {
            if (collision.collider.tag == "Boss")
                bossHit = true;
            StartCoroutine(toBattle());
            Destroy(collision.gameObject);
        }
    }

    void Sprint()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            moveSpeed = 7f;
        }
        else
        {
            moveSpeed = 4f;
        }
    }

    private void Dab()
    {
        if (Input.GetKeyDown("q"))
        {
            state = State.NotMoving;
            anim.SetTrigger("Dab");
            Debug.Log ("Dab");
        }
    }

    private void ReturnToNormal()
    {
        state = State.Normal;
        Debug.Log("You can move now");
    }

    private void NotMoving()
    {
        moveSpeed = 0f;
        movement = Vector2.zero;
    }


    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if(collision.CompareTag("Enemy"))
    //    {
    //        StartCoroutine(toBattle());

    //    }
    //}

    public bool Boss()
    {
        if (bossHit)
            return true;
        else
            return false;
    }

    IEnumerator toBattle()
    {
        characterLock();        fieldtheme.Stop();
        song.Play();
        fading.Play("BattleFade");

        yield return new WaitForSeconds(2.5f);
        //player.SetActive(false);
        battle.SetActive(true);
    }
}