﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Interaction : MonoBehaviour
{
    public TextMeshProUGUI displayText;

    public GameObject dialogue;
    public GameObject popup;

    private bool _interact = false;

    // Start is called before the first frame update
    void Start()
    {
        dialogue.SetActive(false);
        popup.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _interact = Input.GetKeyDown(KeyCode.E);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Hello!");
        if (collider.CompareTag("NPC") || collider.CompareTag("elder"))
        {
            Debug.Log("bruhh");
            popup.SetActive(true);
        }
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.CompareTag("NPC") || collider.CompareTag("elder"))
        {
            if (_interact)
            {
                NPCMessage message = collider.GetComponent<NPCMessage>();
                if (displayText != null)
                {
                    displayText.SetText(message.getMessage());
                    dialogue.SetActive(true);
                    popup.SetActive(false);
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.CompareTag("NPC") || collider.CompareTag("elder"))
        {
            dialogue.SetActive(false);
            popup.SetActive(false);
        }
    }
}
