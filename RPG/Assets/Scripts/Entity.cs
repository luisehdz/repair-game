﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public string entityName;
    public int entityLevel;
    public int damage;
    public int maxHP;
    public int currentHP;
    public int currentMagicCharges;
    public int maxMagicCharges;

    public Animator anim;
    public AudioSource oof;

    public bool takeDamage(int dmg)
    {
        anim.SetTrigger("tookDamage");
        oof.Play();

        currentHP -= dmg;

        if (currentHP <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void heal(int amount)
    {
        currentHP += amount;
        if(currentHP > maxHP)
        {
            currentHP = maxHP;
        }
    }

    public void isAttacking()
    {
        anim.SetTrigger("isAttacking");
    }

    public void usedMagic()
    {
        currentMagicCharges--;
    }

    public int checkMagicCharges()
    {
        return currentMagicCharges;
    }

    public void resetMagic()
    {
        currentMagicCharges = maxMagicCharges;
    }

    public void resetEntity()
    {
        currentHP = maxHP;
    }
}
