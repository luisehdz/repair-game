﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    bool swordCollision;
    bool NPCCollision;
    bool ElderCollision;
    bool bossWall;
    bool bossCollision;

    public bool Sword()
    {
        return swordCollision;
    }
    public bool NPC()
    {
        return NPCCollision;
    }
    public bool Elder()
    {
        return ElderCollision;
    }
    public bool Wall()
    {
        return bossWall;
    }
    public bool Boss()
    {
        return bossCollision;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "sword")
        {
            swordCollision = true;
            collision.gameObject.SetActive(false);
        }
        else if (collision.collider.tag == "elder")
        {
            ElderCollision = true;
        }
        else if (collision.collider.tag == "NPC")
        {
            NPCCollision = true;
        }
        else if (collision.collider.tag == "BossWall")
        {
            bossWall = true;
        }
        else if (collision.collider.tag == "Boss")
        {
            bossCollision = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        ElderCollision = false;
        NPCCollision = false;
        bossWall = false;
        bossCollision = false;
    }
}
